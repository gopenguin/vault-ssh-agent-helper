package main

import (
	"gitlab.com/gopenguin/vault-ssh-agent-helper/cmd"
)

func main() {
	cmd.Execute()
}
