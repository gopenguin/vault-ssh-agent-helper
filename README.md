# Vault ssh-agent Helper

The purpose of this project is to easy usage of the vault ssh certificate engine by automating common workflows.

The main workflow is to generate a temporary ssh key pair, get is signed by vault and add it to a ssh-agent.

```
A helper to generate new keys, get them signed by vault and add them to the ssh agent.

Usage:
  vault-ssh-agent-helper [flags]
  vault-ssh-agent-helper [command]

Available Commands:
  config      get and update the server config
  help        Help about any command
  role        list, create, get and delete ssh roles

Flags:
  -h, --help   help for vault-ssh-agent-helper
```

## Usage

### Preparation
Start [vault](https://www.vaultproject.io/), configure the `VAULT_ADDR` and login via the cli:

```bash
# start vault in dev mode
vault server -dev

# in another terminal
# configure vault address
export VAULT_ADDR="http://localhost:8200"

# login via the cli via the root token
vault login
```

After the initial setup of the vault server you are ready to use the `vault-ssh-agent-helper`. First of all you need an initial configuration. This can be done in two way: by running a vault command or creating manually a kv2 engine and uploading some json.

By command:
```bash
vault-ssh-agent-helper config init
```

Or create a kv engine with the path `ssh/config` and create a secret named `agent` with the following content:
```json
{
  "engineOrder": [],
  "keyType": ""
}
```

To create a new ssh certificate engine you can use the following command:
```bash
vault-ssh-agent-helper role create admin
```

### Signing
To sign a temporary key and add it to the local ssh-agent:
```bash
vault-ssh-agent-helper
```

## Configuration

The effective configuration is a combination of environment variables, server side config and command line arguments.

### Environment Variables

| Name            | Default          | Description               |
|-----------------|------------------|---------------------------|
| VAULT_ADDR      | https://localhost:4200 | the API of the vault server |

