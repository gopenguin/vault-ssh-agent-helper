package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/gopenguin/vault-ssh-agent-helper/internal/command"
	"os"
)

var commander = command.NewCommander()

var (
	roleName string

	rootCmd = &cobra.Command{
		Use:   "vault-ssh-agent-helper",
		Short: "A helper to ease vault ssh interaction",
		Long:  `A helper to generate new keys, get them signed by vault and add them to the ssh agent.`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return commander.RefreshSignedKey(roleName)
		},
	}
)

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().StringVar(&roleName, "role", "", "role to choose for key signing")

	rootCmd.AddCommand(
		configCmd,
		roleCmd,
	)
}
