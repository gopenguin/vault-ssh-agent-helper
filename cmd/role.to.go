package cmd

import "github.com/spf13/cobra"

var (
	roleCmd = &cobra.Command{
		Use:   "role",
		Short: "list, create, get and delete ssh roles",
	}
	roleListCmd = &cobra.Command{
		Use:   "list",
		Short: "list available roles",
		RunE: func(cmd *cobra.Command, args []string) error {
			return commander.RoleList()
		},
	}
	roleCreateCmd = &cobra.Command{
		Use:   "create [name]...",
		Short: "create a new role",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			for _, name := range args {
				commander.RoleCreate(name)
			}
		},
	}
	roleDeleteCmd = &cobra.Command{
		Use:   "delete [name]...",
		Short: "delete a role",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			for _, name := range args {
				commander.RoleDelete(name)
			}
		},
	}
)

func init() {
	roleCmd.AddCommand(
		roleListCmd,
		roleCreateCmd,
		roleDeleteCmd,
	)
}
