package cmd

import (
	"github.com/spf13/cobra"
)

var (
	configCmd = &cobra.Command{
		Use:   "config",
		Short: "get and update the server config",
	}

	configInitCmd = &cobra.Command{
		Use:   "init",
		Short: "create a initial configuration in vault",
		Run: func(cmd *cobra.Command, args []string) {
			commander.InitConfig()
		},
	}

	configGetCmd = &cobra.Command{
		Use:   "get",
		Short: "Get the server side config",
		Run: func(cmd *cobra.Command, args []string) {
			commander.PrintConfig()
		},
	}

	configFile string

	configSetCmd = &cobra.Command{
		Use:   "set",
		Short: "Update the server side config",
		Run: func(cmd *cobra.Command, args []string) {
			commander.SetConfig(configFile)
		},
	}
)

func init() {
	configSetCmd.Flags().StringVarP(&configFile, "file", "f", "", "the file to upload")

	configCmd.AddCommand(
		configInitCmd,
		configGetCmd,
		configSetCmd,
	)
}
