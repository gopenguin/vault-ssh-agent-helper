module gitlab.com/gopenguin/vault-ssh-agent-helper

go 1.13

require (
	github.com/Microsoft/go-winio v0.4.15-0.20190919025122-fc70bd9a86b5
	github.com/hashicorp/vault v1.5.3
	github.com/hashicorp/vault/api v1.0.5-0.20200630205458-1a16f3c699c6
	github.com/mitchellh/mapstructure v1.3.2
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v0.0.5
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9
)

replace github.com/hashicorp/vault/api => github.com/hashicorp/vault/api v1.0.4
