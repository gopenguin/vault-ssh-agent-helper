package vault

import (
	"github.com/hashicorp/vault/api"
	vaultToken "github.com/hashicorp/vault/command/token"
	"github.com/pkg/errors"
	"golang.org/x/crypto/ssh"
	"sort"
	"strings"
)

type Client struct {
	c              *api.Client
	config         Config
	selectedEngine string
}

func NewClient() (*Client, error) {
	c, err := newClientInternal()
	if err != nil {
		return nil, errors.Wrap(err, "unable to create vault client")
	}

	client := &Client{c: c}
	err = client.loadToken()
	if err != nil {
		return nil, errors.Wrap(err, "unable to load vault token")
	}

	err = client.loadConfig()
	if err != nil {
		return client, errors.Wrap(err, "unable to load vault server side config")
	}

	err = client.findEngine()
	if err != nil {
		return client, errors.Wrap(err, "unable to find ssh signing engine")
	}

	return client, nil
}

func newClientInternal() (*api.Client, error) {
	config := api.DefaultConfig()
	client, err := api.NewClient(config)
	return client, err
}

func (c *Client) loadToken() (err error) {
	// Get the token if it came in from the environment
	token := c.c.Token()

	// If we don't have a token, check the token helper
	if token == "" {
		helper, err := vaultToken.NewInternalTokenHelper()
		if err != nil {
			return err
		}

		token, err = helper.Get()
		if err != nil {
			return err
		}
	}

	if token != "" {
		c.c.SetToken(token)
	}

	return nil
}

func (c *Client) loadConfig() error {
	config, err := c.GetConfig()
	if err != nil {
		return err
	}

	c.config = config
	return nil
}

func (c Client) Config() Config {
	return c.config
}

func (c *Client) findEngine() error {
	mounts, err := c.SshMounts()
	if err != nil {
		return err
	}

	c.selectedEngine = mounts[0]
	return nil
}

func (c *Client) SshMounts() ([]string, error) {
	mounts, err := c.c.Sys().ListMounts()
	if err != nil {
		return nil, errors.Wrap(err, "unable to list mounts")
	}

	var sshMounts []string
	for path, mountOpts := range mounts {
		if mountOpts.Type == "ssh" {
			sshMounts = append(sshMounts, path)
		}
	}

	if len(sshMounts) == 0 {
		return nil, errors.New("no ssh mounts found")
	}

	mountIndex := make(map[string]int)
	for i, engine := range c.config.EngineOrder {
		mountIndex[engine] = i
	}

	sort.Slice(sshMounts, mountCompare(sshMounts, mountIndex))

	return sshMounts, nil
}

func mountCompare(mounts []string, mountsIndex map[string]int) func(int, int) bool {
	return func(i, j int) bool {
		a, b := mounts[i], mounts[j]
		a = stripSshMountPrefix(a)
		b = stripSshMountPrefix(b)

		aIndex, aOk := mountsIndex[a]
		bIndex, bOk := mountsIndex[b]

		if aOk && bOk {
			return aIndex < bIndex
		} else if aOk {
			return true
		} else if bOk {
			return false
		} else {
			return strings.Compare(a, b) < 0
		}
	}
}

func stripSshMountPrefix(path string) string {
	return strings.TrimPrefix(path, "ssh/")
}

func toSshMountPath(roleName string) string {
	return "ssh/" + roleName + "/"
}

func (c *Client) SignKey(roleName string, key ssh.PublicKey) (*ssh.Certificate, error) {
	var mountPath string
	if roleName == "" {
		mountPath = c.selectedEngine
	} else {
		mountPath = toSshMountPath(roleName)
	}
	sshMount := c.c.SSHWithMountPoint(mountPath)

	publicKeyStr := string(ssh.MarshalAuthorizedKey(key))

	signedKeyData, err := sshMount.SignKey("default", map[string]interface{}{
		// WARNING: publicKey is []byte, which is b64 encoded on JSON upload. We
		// have to convert it to a string. SV lost many hours to this...
		"public_key": publicKeyStr,
		"cert_type":  "user",

		// TODO: let the user configure these. In the interim, if users want to
		// customize these values, they can produce the key themselves.
		"extensions": map[string]string{
			"permit-X11-forwarding":   "",
			"permit-agent-forwarding": "",
			"permit-port-forwarding":  "",
			"permit-pty":              "",
			"permit-user-rc":          "",
		},
	})
	if err != nil {
		return nil, err
	}

	if signedKeyData == nil || signedKeyData.Data == nil {
		return nil, errors.New("missing signed key")
	}

	// Extract public key
	signedKeyStr, ok := signedKeyData.Data["signed_key"].(string)
	if !ok || signedKeyStr == "" {
		return nil, errors.New("signed key is empty")
	}

	signedKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(signedKeyStr))
	if err != nil {
		return nil, errors.Wrap(err, "unable to parse signed key")
	}

	cert, ok := signedKey.(*ssh.Certificate)
	if !ok {
		return nil, errors.New("unable to extract certificate")
	}

	return cert, nil
}

func SignKey(publicKey string) (string, error) {
	config := api.DefaultConfig()
	client, err := api.NewClient(config)
	if err != nil {
		return "", err
	}

	// Get the token if it came in from the environment
	token := client.Token()

	// If we don't have a token, check the token helper
	if token == "" {
		helper := &vaultToken.InternalTokenHelper{}
		token, err = helper.Get()
		if err != nil {
			return "", err
		}
	}

	if token != "" {
		client.SetToken(token)
	}

	mounts, err := client.Sys().ListMounts()
	if err != nil {
		return "", err
	}

	var sshMounts []string
	for path, mountOpts := range mounts {
		if mountOpts.Type == "ssh" {
			sshMounts = append(sshMounts, path)
		}
	}

	if len(sshMounts) == 0 {
		return "", errors.New("no ssh mounts found")
	}

	sshMount := client.SSHWithMountPoint(sshMounts[0])
	signedKeyData, err := sshMount.SignKey("default", map[string]interface{}{
		// WARNING: publicKey is []byte, which is b64 encoded on JSON upload. We
		// have to convert it to a string. SV lost many hours to this...
		"public_key": publicKey,
		"cert_type":  "user",

		// TODO: let the user configure these. In the interim, if users want to
		// customize these values, they can produce the key themselves.
		"extensions": map[string]string{
			"permit-X11-forwarding":   "",
			"permit-agent-forwarding": "",
			"permit-port-forwarding":  "",
			"permit-pty":              "",
			"permit-user-rc":          "",
		},
	})
	if err != nil {
		return "", err
	}

	if signedKeyData == nil || signedKeyData.Data == nil {
		return "", errors.New("missing signed key")
	}

	// Extract public key
	signedKey, ok := signedKeyData.Data["signed_key"].(string)
	if !ok || signedKey == "" {
		return "", errors.New("signed key is empty")
	}

	return signedKey, nil
}
