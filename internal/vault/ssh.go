package vault

import (
	"fmt"
	"github.com/hashicorp/vault/api"
	"log"
)

func (c *Client) CreateSshMount(name string) {
	mountPath := mountPath(name)
	err := c.c.Sys().Mount(mountPath, &api.MountInput{
		Type:        "ssh",
		Description: "",
		Config: api.MountConfigInput{
			DefaultLeaseTTL: "30m",
			MaxLeaseTTL:     "8h",
		},
		Local:                 false,
		SealWrap:              false,
		Options:               nil,
	})
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("mounted ssh engine on '%s'\n", mountPath)

	err = c.createSshCert(mountPath)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("generated ssh signing key")

	err = c.createSshRoleDefault(mountPath)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("created 'default' signing role")

	err = c.createSshPolicy(mountPath)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("created signing policy '%s'\n", mountPath)

	fmt.Printf("created role '%s'\n", name)
}

func mountPath(name string) string {
	return fmt.Sprintf("ssh/%s", name)
}

func (c *Client) createSshCert(path string) error {
	_, err := c.c.Logical().Write(path+"/config/ca", map[string]interface{}{
		"generate_signing_key": true,
	})

	return err
}

func (c *Client) createSshRoleDefault(path string) error {
	_, err := c.c.Logical().Write(path+"/roles/default", map[string]interface{}{
		"key_type":                "ca",
		"allow_user_certificates": true,
		"allowed_users":           "*",
		"ttl":                     "30m",
		"max_ttl":                 "8h",
	})

	return err
}

func (c *Client) createSshPolicy(path string) error {
	return c.c.Sys().PutPolicy(
		path,
		"path \""+path+"/sign/default\" {\n"+
			"  capabilities = [\"create\", \"update\"]\n"+""+
			"}\n"+
			"\n"+
			"path \""+path+"/roles\" {\n"+
			"  capabilities = [\"list\"]\n"+
			"}\n",
	)
}

func (c *Client) DeleteSshMount(name string) {
	path := mountPath(name)

	fmt.Printf("deleting role '%s'\n", name)
	err := c.c.Sys().Unmount(path)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("deleting policy '%s'\n", path)
	err = c.c.Sys().DeletePolicy(path)
	if err != nil {
		fmt.Println(err)
	}

}
