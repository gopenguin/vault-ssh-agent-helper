package vault

import (
	"reflect"
	"sort"
	"testing"
)

func Test_mountCompare(t *testing.T) {
	type args struct {
		mounts      []string
		mountsIndex map[string]int
	}
	type testStatement struct {
		i, j   int
		result bool
	}
	tests := []struct {
		name      string
		args      args
		want      []testStatement
		wantFinal []string
	}{
		{
			name: "both in map",
			args: args{
				mounts:      []string{"ssh/admin", "ssh/test"},
				mountsIndex: map[string]int{"admin": 0, "test": 1},
			},
			want: []testStatement{
				{
					i:      0,
					j:      0,
					result: false,
				},
				{
					i:      0,
					j:      1,
					result: true,
				},
			},
			wantFinal: []string{"ssh/admin", "ssh/test"},
		},
		{
			name: "admin not in map",
			args: args{
				mounts:      []string{"ssh/admin", "ssh/test"},
				mountsIndex: map[string]int{"test": 1},
			},
			want: []testStatement{
				{
					i:      0,
					j:      0,
					result: false,
				},
				{
					i:      0,
					j:      1,
					result: false,
				},
				{
					i:      1,
					j:      0,
					result: true,
				},
			},
			wantFinal: []string{"ssh/test", "ssh/admin"},
		},
		{
			name: "both not in map",
			args: args{
				mounts:      []string{"ssh/admin", "ssh/test"},
				mountsIndex: map[string]int{},
			},
			want: []testStatement{
				{
					i:      0,
					j:      0,
					result: false,
				},
				{
					i:      0,
					j:      1,
					result: true,
				},
			},
			wantFinal: []string{"ssh/admin", "ssh/test"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := mountCompare(tt.args.mounts, tt.args.mountsIndex)

			for _, test := range tt.want {
				gotResult := got(test.i, test.j)
				if test.result != gotResult {
					t.Errorf("mountCompare()(%d, %d) = %v, want %v", test.i, test.j, gotResult, test.result)
				}
			}

			gotFinal := append(tt.args.mounts[:0:0], tt.args.mounts...)
			sort.Slice(gotFinal, got)
			if !reflect.DeepEqual(tt.wantFinal, gotFinal) {
				t.Errorf("sort.Slice(mounts, mountCompare()) = %v, want %v", gotFinal, tt.wantFinal)
			}
		})
	}
}
