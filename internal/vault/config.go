package vault

import (
	"encoding/json"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
)

const (
	serverSideConfigMount = "ssh/config/"
	serverSideConfigPath  = serverSideConfigMount + "data/agent"
)

type Config struct {
	EngineOrder []string `json:"engineOrder"`
	KeyType     string   `json:"keyType"`
}

func (c Config) String() string {
	str, err := json.Marshal(c)
	if err != nil {
		return err.Error()
	}

	return string(str)
}

func (c *Client) EnsureConfigKv() error {
	mounts, err := c.c.Sys().ListMounts()
	if err != nil {
		return errors.Wrap(err, "unable to list server mounts")
	}

	_, ok := mounts[serverSideConfigMount]
	if ok {
		return nil
	}

	err = c.c.Sys().Mount(serverSideConfigMount, &api.MountInput{
		Type:    "kv",
		Options: map[string]string{"version": "2"},
		Config:  api.MountConfigInput{},
	})
	if err != nil {
		return errors.Wrapf(err, "unable to mount kv2 at '%s'", serverSideConfigMount)
	}

	return nil
}

func (c *Client) GetConfig() (Config, error) {
	configData, err := c.c.Logical().Read(serverSideConfigPath)
	if err != nil || configData == nil {
		return Config{}, nil
	}

	var config Config
	err = mapstructure.Decode(configData.Data["data"], &config)
	if err != nil {
		return Config{}, errors.Wrap(err, "unable to decode server config")
	}

	return config, nil
}

func (c *Client) UpdateConfig(config Config) error {
	data := map[string]interface{}{
		"data": config,
	}

	_, err := c.c.Logical().Write(serverSideConfigPath, data)

	return err
}
