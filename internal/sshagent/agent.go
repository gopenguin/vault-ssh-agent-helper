package sshagent

import (
	"fmt"
	"golang.org/x/crypto/ssh/agent"
	"log"
)

type agentFactory func() (agent.Agent, error)

var agentFactories = make(map[string]agentFactory)

func registerFactory(name string, factory agentFactory) {
	agentFactories[name] = factory
}

func createAgents() ([]agent.Agent, error) {
	var agents []agent.Agent

	for name, factory := range agentFactories {
		a, err := factory()
		if err != nil {
			log.Printf("unable to create agent client for '%s': %v\n", name, err)
		}

		fmt.Printf("found agent '%s'\n", name)
		agents = append(agents, a)
	}

	return agents, nil
}
