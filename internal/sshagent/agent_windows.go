// +build windows

package sshagent

import (
	winio "github.com/Microsoft/go-winio"
	"golang.org/x/crypto/ssh/agent"
)

func init() {
	registerFactory("windows ssh agent", newWindowsAgent)
}

func newWindowsAgent() (agent.Agent, error) {
	conn, err := winio.DialPipe(`\\.\pipe\openssh-ssh-agent`, nil)
	if err != nil {
		return nil, err
	}

	return agent.NewClient(conn), nil
}
