// +build !windows

package sshagent

import (
	"github.com/pkg/errors"
	"golang.org/x/crypto/ssh/agent"
	"net"
	"os"
)

func init() {
	registerFactory("unix socket", newUnixSocketClient)
}

func newUnixSocketClient() (agent.Agent, error) {
	// ssh-agent(1) provides a UNIX socket at $SSH_AUTH_SOCK.
	socket := os.Getenv("SSH_AUTH_SOCK")
	conn, err := net.Dial("unix", socket)
	if err != nil {
		return nil, errors.Wrap(err, "failed to open SSH_AUTH_SOCK")
	}

	return agent.NewClient(conn), nil
}
