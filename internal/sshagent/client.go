package sshagent

import (
	"github.com/pkg/errors"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"time"
)

type Client struct {
	agents []agent.Agent
}

func NewClient() (*Client, error) {
	agents, err := createAgents()
	if err != nil {
		return nil, err
	}

	return &Client{agents: agents}, nil
}

func (c *Client) AddKey(key interface{}, cert *ssh.Certificate) error {
	lifetimeSecs := uint32(cert.ValidBefore - uint64(time.Now().Unix()))

	for _, a := range c.agents {
		err := a.Add(agent.AddedKey{
			PrivateKey:           key,
			Certificate:          cert,
			Comment:              "",
			LifetimeSecs:         lifetimeSecs,
			ConfirmBeforeUse:     false,
			ConstraintExtensions: nil,
		})

		if err != nil {
			return errors.Wrap(err, "failed to add key to agent")
		}
	}

	return nil
}
