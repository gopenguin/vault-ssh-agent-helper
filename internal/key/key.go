package key

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"github.com/pkg/errors"
)

const DefaultKeyType = "ed25519"

type keyGenerator func() (interface{}, interface{}, error)

var keyGenerators = map[string]keyGenerator{
	"rsa-4096":  newRsa(4096),
	"ecdsa-256": newEcdsa(elliptic.P256()),
	"ecdsa-384": newEcdsa(elliptic.P384()),
	"ecdsa-521": newEcdsa(elliptic.P521()),
	"ed25519":   newEd25519,
}

func New(keyType string) (interface{}, interface{}, error) {
	if keyType == "" {
		keyType = DefaultKeyType
	}

	generator, ok := keyGenerators[keyType]
	if !ok {
		return nil, nil, errors.New(fmt.Sprintf("unsupported key type: %s", keyType))
	}

	return generator()
}

func newRsa(keySize int) keyGenerator {
	return func() (interface{}, interface{}, error) {
		key, err := rsa.GenerateKey(rand.Reader, keySize)
		if err != nil {
			return nil, nil, err
		}
		return key.Public(), key, nil
	}
}

func newEcdsa(curve elliptic.Curve) keyGenerator {
	return func() (interface{}, interface{}, error) {
		key, err := ecdsa.GenerateKey(curve, rand.Reader)
		if err != nil {
			return nil, nil, err
		}
		return key.Public(), key, nil
	}
}

func newEd25519() (interface{}, interface{}, error) {
	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	return pub, &priv, err
}
