package command

import "fmt"

func (c *commander) RoleList() error {
	mounts, err := c.vault().SshMounts()
	if err != nil {
		return err
	}

	for _, mount := range mounts {
		fmt.Println(mount)
	}
	return nil
}

func (c *commander) RoleCreate(name string) {
	c.vault().CreateSshMount(name)
}

func (c *commander) RoleDelete(name string) {
	c.vault().DeleteSshMount(name)
}
