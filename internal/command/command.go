package command

import (
	"gitlab.com/gopenguin/vault-ssh-agent-helper/internal/sshagent"
	"gitlab.com/gopenguin/vault-ssh-agent-helper/internal/vault"
	"log"
	"sync"
)

type commander struct {
	vaultOnce sync.Once
	vaultRef  *vault.Client

	sshOnce sync.Once
	sshRef  *sshagent.Client
}

func NewCommander() *commander {
	return &commander{}
}

func (c *commander) vault() *vault.Client {
	c.vaultOnce.Do(func() {
		vaultClient, err := vault.NewClient()
		if err != nil {
			if vaultClient == nil {
				log.Fatalln(err)
			} else {
				log.Println(err)
			}
		}
		c.vaultRef = vaultClient
	})

	return c.vaultRef
}

func (c *commander) ssh() *sshagent.Client {
	c.sshOnce.Do(func() {
		agentClient, err := sshagent.NewClient()
		if err != nil {
			panic(err)
		}
		c.sshRef = agentClient
	})

	return c.sshRef
}
