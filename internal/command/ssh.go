package command

import (
	"gitlab.com/gopenguin/vault-ssh-agent-helper/internal/key"
	"golang.org/x/crypto/ssh"
)

func (c *commander) RefreshSignedKey(roleName string) error {
	vaultClient := c.vault()

	config := vaultClient.Config()
	pub, priv, err := key.New(config.KeyType)
	if err != nil {
		return err
	}

	publicKey, err := ssh.NewPublicKey(pub)
	if err != nil {
		return err
	}

	cert, err := vaultClient.SignKey(roleName, publicKey)
	if err != nil {
		return err
	}

	return c.ssh().AddKey(priv, cert)
}
