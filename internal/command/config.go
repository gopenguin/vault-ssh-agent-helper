package command

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/gopenguin/vault-ssh-agent-helper/internal/vault"
	"io/ioutil"
	"log"
	"os"
)

func (c *commander) PrintConfig() {
	fmt.Println(c.vault().Config())
}

func (c *commander) SetConfig(configFileStr string) {
	configFile, err := os.Open(configFileStr)
	if err != nil {
		log.Fatalln(errors.Wrap(err, "failed opening file"))
	}

	configBytes, err := ioutil.ReadAll(configFile)
	if err != nil {
		log.Fatalln(errors.Wrap(err, "failed reading file"))
	}

	var config vault.Config
	err = json.Unmarshal(configBytes, &config)
	if err != nil {
		log.Fatalln(err)
	}

	err = c.vault().UpdateConfig(config)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("configuration updated successfully")
}

func (c *commander) InitConfig() {
	err := c.vault().EnsureConfigKv()
	if err != nil {
		log.Fatalln(err)
	}

	var config vault.Config
	err = c.vault().UpdateConfig(config)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("configuration initialized successfully")

}
